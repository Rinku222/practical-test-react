import { useEffect, useState } from "react";
import axios from "axios";
import { Grid, makeStyles, Paper, Typography } from "@material-ui/core";
import Moment from "moment";

const DATA_URL = "https://randomuser.me/api/?page=1&results=30";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(3),
    margin: 5,
  },
}));

function App() {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});

  const classes = useStyles();

  // Function to fetch data from api
  const fetchRandomData = () => {
    return axios
      .get(DATA_URL)
      .then((res) => {
        setData(res);
      })
      .catch((err) => {
        console.log("----->err", err);
      });
  };

  useEffect(() => {
    fetchRandomData();
    setLoading(false);
  }, []);

  return (
    <div className={classes.root}>
      {loading ? (
        <div>...loading...</div>
      ) : (
        <Grid container direction="column">
          {data?.data?.results.map((value, index) => {
            return (
              <Grid item>
                <Paper className={classes.paper}>
                  <Grid container spacing={2}>
                    <Grid item>
                      <img src={`${value.picture.thumbnail}`} />
                    </Grid>
                    <Grid item>
                      <Grid container direction="column">
                        <Grid item>
                          <Grid container spacing={1}>
                            <Grid item>Name:</Grid>
                            <Grid item>{value?.name.first}</Grid>
                            <Grid item>{value?.name.last}</Grid>
                          </Grid>
                        </Grid>
                        <Grid item>Email Id: {value.email}</Grid>
                        <Grid item>
                          DOB: {Moment(value.dob.date).format("DD-MM-YYYY")}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            );
          })}
        </Grid>
      )}
    </div>
  );
}

export default App;
